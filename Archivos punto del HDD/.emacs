(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(browse-url-firefox-program "/home/carlos/Descargas/waterfox/waterfox")
 '(browse-url-generic-program "/home/carlos/Descargas/waterfox/waterfox")
 '(custom-enabled-themes (quote (misterioso)))
 '(desktop-save-mode t)
 '(doc-view-continuous t)
 '(doc-view-resolution 200)
 '(elfeed-feeds
   (quote
    ("http://planet.emacs-es.org/rss20.xml" "https://www.youtube.com/feeds/videos.xml?channel_id=UCEDQrpgx5maz2Nz5uxb4Aow" "https://lamiradadelreplicante.com/feed/" "https://www.youtube.com/feeds/videos.xml?channel_id=UCDy1lHSVGBDL0d2_C5NZNJw" "https://victorhckinthefreeworld.com/feed/" "https://www.youtube.com/feeds/videos.xml?channel_id=UC1_uAIS3r8Vu6JjXWvastJg" "https://www.youtube.com/feeds/videos.xml?channel_id=UCPmJ0InUBZSTTCy_vVsdXxQ" "https://www.youtube.com/feeds/videos.xml?channel_id=UCRg7_He-X772bG6od0xkyug" "https://www.youtube.com/feeds/videos.xml?channel_id=UCXmVar2gZRlPdMyDyzOe4sQ" "https://www.youtube.com/feeds/videos.xml?channel_id=UCDZIBAWM-5ZU5vMo83Oq8cg" "https://www.youtube.com/feeds/videos.xml?channel_id=UCqTVfT9JQqhA6_Hi_h_h97Q" "https://www.youtube.com/feeds/videos.xml?channel_id=UC1_uAIS3r8Vu6JjXWvastJg" "https://www.youtube.com/feeds/videos.xml?channel_id=UCczx_6m8ZWwcntGSd3FV3mQ" "https://www.youtube.com/feeds/videos.xml?channel_id=UCEKoup54hd2PdEg8WaMQslw" "https://www.youtube.com/feeds/videos.xml?channel_id=UCczx_6m8ZWwcntGSd3FV3mQ")))
 '(global-visual-line-mode t)
 '(inhibit-startup-screen t)
 '(large-file-warning-threshold 1000000000)
 '(line-number-mode nil)
 '(next-screen-context-lines 4)
 '(save-place t nil (saveplace))
 '(yahoo-weather-location "Mairena del Aljarafe")
 '(yahoo-weather-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-string-face ((t (:foreground "dark orange")))))
(require 'org);;Config para org mode
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
;;(define-key c-mode-map  "\C-ñ" 'compile)
(setq org-log-done t)

(package-initialize)

(setq org-agenda-files (list "~/org/uni.org"))
;; a ver si condigo darle definición a los pdfs

(require 'doc-view)
(setq doc-view-resolution 144)
(require 'xscheme)
(server-start)


(setq package-archives
'(("gnu" . "http://elpa.gnu.org/packages/")
("melpa" . "http://melpa.milkbox.net/packages/")))

(pdf-tools-install)
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(ido-mode 1)
(setq ido-everywhere t)
(setq ido-enable-flex-matching t)

;;Ponemos los mensajes que salen en otro búfer en el mismo
(tooltip-mode -1)
(setq tooltip-use-echo-area t)


;;Configuración para acceder fácil a archivos recientes con recentf

 
(require 'recentf)

;; get rid of `find-file-read-only' and replace it with something
;; more useful.
(global-set-key (kbd "C-x C-r") 'ido-recentf-open)
(global-set-key (kbd "C-ñ") 'eval-buffer)

;; enable recent files mode.
(recentf-mode t)

; 50 files ought to be enough.
(setq recentf-max-saved-items 50)

(defun ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

;;Ponemos M-o para cambiar de búfer cuando hay varios abiertos

(global-set-key (kbd "M-ñ") 'other-window)
(global-set-key (kbd"M-o") 'mode-line-other-buffer)
(global-set-key (kbd "<f7>") 'bookmark-jump)
(global-set-key (kbd "<f6>") 'bookmark-set)

(setq confirm-kill-emacs 'y-or-n-p) ;; Pedir confirmación para salir de emacs

(setq desktop-save-mode t) ;; guardar la sessión al cerrar emacs y restaurarla

(desktop-save-mode 1) ;; guardar sesión emacs

;; Tell emacs where is your personal elisp lib dir
(add-to-list 'load-path "~/.emacs.d/lisp/")

;; cargamos el diccionario con sus atajos de teclado
(load "define-word")

(global-set-key (kbd "C-c D") 'define-word-at-point)
(global-set-key (kbd "C-c d") 'define-word)


;;Mostramos el número de fila para programar mejor

(add-hook 'prog-mode-hook 'linum-mode)

;;Matar buffer con C-x-K
(defun other-window-kill-buffer ()
  "Kill the buffer in the other window"
  (interactive)
  ;; Window selection is used because point goes to a different window
  ;; if more than 2 windows are present
  (let ((win-curr (selected-window))
        (win-other (next-window)))
    (select-window win-other)
    (kill-this-buffer)
    (select-window win-curr)))

(global-set-key (kbd "C-x K") 'other-window-kill-buffer)

;;Prueba de smooth scrolling
;;(setq scroll-conservatively 10000)
(setq redisplay-dont-pause t
  scroll-margin 1
  scroll-step 1
  scroll-conservatively 10000
  scroll-preserve-screen-position 1)

;;Ver el tiempo con wttrin
(setq wttrin-default-accept-language '("Accept-Language" . "es-ES"))
(setq wttrin-default-cities '("Mairena" "mairena" "Sevilla"))
;;(yahoo-weather-mode)

;;Elfeed para RSS

(setq elfeed-feeds
      '("https://www.youtube.com/feeds/videos.xml?channel_id=UCEDQrpgx5maz2Nz5uxb4Aow"
	"http://planet.emacs-es.org/rss20.xml"
	"http://planet.emacsen.org/atom.xml"
	"https://lamiradadelreplicante.com/feed/"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCDy1lHSVGBDL0d2_C5NZNJw"
	"https://victorhckinthefreeworld.com/feed/"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UC1_uAIS3r8Vu6JjXWvastJg"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCPmJ0InUBZSTTCy_vVsdXxQ"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCRg7_He-X772bG6od0xkyug"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCXmVar2gZRlPdMyDyzOe4sQ"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCDZIBAWM-5ZU5vMo83Oq8cg"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCqTVfT9JQqhA6_Hi_h_h97Q"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UC1_uAIS3r8Vu6JjXWvastJg"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCczx_6m8ZWwcntGSd3FV3mQ"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCEKoup54hd2PdEg8WaMQslw"
	"https://www.youtube.com/feeds/videos.xml?channel_id=UCczx_6m8ZWwcntGSd3FV3mQ"
	"https://twobithistory.org/feed.xml"
	))

;;Función para ver feeds de elfeed con mpv

;; --------------------EMPIEZA ELFEED--------------------------------
(setq elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory))

(defun ambrevar/elfeed-play-with-mpv ()
  "Play entry link with mpv."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single)))
        (quality-arg "")
        (quality-val (completing-read "Max height resolution (0 for unlimited): " '("0" "480" "720") nil nil)))
    (setq quality-val (string-to-number quality-val))
    (message "Opening %s with height≤%s with mpv..." (elfeed-entry-link entry) quality-val)
    (when (< 0 quality-val)
      (setq quality-arg (format "--ytdl-format=[height<=?%s]" quality-val)))
    (start-process "elfeed-mpv" nil "mpv" quality-arg (elfeed-entry-link entry))))
(define-key global-map "\C-co" 'ambrevar/elfeed-play-with-mpv)

(defun ambrevar/elfeed-open-with-eww ()
  "Open in eww with `eww-readable'."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode) elfeed-show-entry (elfeed-search-selected :single))))
    (eww  (elfeed-entry-link entry))
    (add-hook 'eww-after-render-hook 'eww-readable nil t)))

(defvar ambrevar/elfeed-visit-patterns
  '(("youtu\\.?be" . ambrevar/elfeed-play-with-mpv)
    ("phoronix" . ambrevar/elfeed-open-with-eww))
  "List of (regexps . function) to match against elfeed entry link to know
whether how to visit the link.")

(defun ambrevar/elfeed-visit-maybe-external ()
  "Visit with external function if entry link matches `ambrevar/elfeed-visit-patterns',
visit otherwise."
  (interactive)
  (let ((entry (if (eq major-mode 'elfeed-show-mode)
                   elfeed-show-entry
                 (elfeed-search-selected :single)))
        (patterns ambrevar/elfeed-visit-patterns))
    (while (and patterns (not (string-match (caar patterns) (elfeed-entry-link entry))))
      (setq patterns (cdr patterns)))
    (cond
     (patterns
      (funcall (cdar patterns)))
     ((eq major-mode 'elfeed-search-mode)
      (call-interactively 'elfeed-search-show-entry))
     (t (elfeed-show-visit)))))

;;(define-key elfeed-search-mode-map "v" #'elfeed-play-in-mpv)

(defun ambrevar/elfeed-kill-entry ()
  "Like `elfeed-kill-buffer' but pop elfeed search."
  (interactive)
  (elfeed-kill-buffer)
  (switch-to-buffer "*elfeed-search*"))
;;(define-key elfeed-show-mode-map "q" #'ambrevar/elfeed-kill-entry)

(defun ambrevar/elfeed-switch-back ()
  "Back to the last elfeed buffer, entry or search."
  (interactive)
  (let ((buffer (get-buffer "*elfeed-entry*")))
    (if buffer
        (switch-to-buffer buffer)
      (elfeed))))

(load "~/personal/news/elfeed.el" t)

(provide 'init-elfeed)

;;Prueba para actualizar elfeed cada media hora
(run-with-timer 0 (* 30 60) 'elfeed-update)

;; --------------------ACABA ELFEED--------------------------------

;;Configuración de EMMS

        (add-to-list 'load-path "~/emms/lisp")
        (require 'emms-setup)
        (emms-default-players)
        (setq emms-source-file-default-directory "~/Música/")

;;Prueba de crear alias
(defalias  'tiempo 'wttrin)

;;Helm mode
(require 'helm-config)
;; telegram
 (use-package telega
   :load-path  "~/telega.el"
   :commands (telega)
   :defer t)
(add-hook 'telega-root-mode-hook (lambda () (telega-notifications-mode 1)))
 (add-to-list 'load-path "~/Plantillas/telega.el")
(require 'telega)
